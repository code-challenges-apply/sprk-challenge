# Run code on a machine with docker and docker-compose 

1. build image (**Remember to build new image everytime you update the code**)

```bash
docker-compose build
```

2. run

```bash
docker-compose up
```

Now you can create a dummy data via `http://localhost:8000/v1/api/product/` from the following data:

```json
{
  "amounts":[
    {
      "amount":3,
      "bbd":"2021-09-30T00:00:00Z",
      "comment":"",
      "country_of_disassembly":null,
      "country_of_rearing":null,
      "country_of_slaughter":null,
      "cutting_plant_registration":null,
      "item":{
        "amount_multiplier":24,
        "brand":"Unknown",
        "categ_id":750,
        "category_id":"10000236",
        "code":"4311527",
        "description":"Waln\u00fcsse idS 30mm+ FR I 500g BT",
        "edeka_article_number":"80062875",
        "gross_weight":0,
        "id":"2434",
        "net_weight":12000,
        "notes":false,
        "packaging":"PUG",
        "related_products":[

        ],
        "requires_best_before_date":true,
        "requires_meat_info":false,
        "trade_item_descriptor":"CASE",
        "trade_item_unit_descriptor_name":"Karton",
        "type":"gtin",
        "unit_name":"g",
        "validation_status":"validated"
      },
      "lot_number":null,
      "slaughterhouse_registration":null
    }
  ],
  "session_end_time":"2022-04-29T11:56:37.132Z",
  "session_id":1651233397132,
  "session_start_time":"2022-04-29T11:40:14.860Z",
  "supplier_id":"1050",
  "user_id":"adrian@sprk.global"
}
```

Now you can see the list of products from `http://localhost:8000/v1/api/product/`. You also can filter a product by code `http://localhost:8000/v1/api/product/?code=`



You also can add a new super user by running `docker-compose run django-api python manage.py createsuperuser`. Then login via `http://localhost:8000/admin/`. 

# Test codes
Run the following command on a machine with docker and docker-compose

```bash
docker-compose run django-api ./entry.sh test
```