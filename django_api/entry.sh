#!/bin/sh

set -e

orig=$@
cmd=$1
shift

if [ $cmd = 'web' ]; then
  python manage.py migrate
  python manage.py collectstatic --no-input
  exec gunicorn django_api.wsgi -c gunicorn.py --reload
elif [ $cmd = 'test' ]; then
  exec python manage.py test
fi

exec "$orig"