# Generated by Django 4.1.7 on 2023-03-02 17:39

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Product",
            fields=[
                (
                    "id",
                    models.UUIDField(
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                    ),
                ),
                ("amount_multiplier", models.IntegerField()),
                ("brand", models.CharField(max_length=255)),
                ("categ_id", models.IntegerField()),
                ("category_id", models.CharField(max_length=255)),
                ("code", models.CharField(max_length=14)),
                ("description", models.TextField()),
                ("edeka_article_number", models.CharField(max_length=255)),
                ("gross_weight", models.FloatField()),
                ("net_weight", models.FloatField()),
                ("notes", models.BooleanField()),
                ("packaging", models.CharField(max_length=255)),
                ("requires_best_before_date", models.BooleanField()),
                ("requires_meat_info", models.BooleanField()),
                ("trade_item_unit_descriptor", models.CharField(max_length=255)),
                ("trade_item_unit_descriptor_name", models.CharField(max_length=255)),
                ("type", models.CharField(max_length=255)),
                ("unit_name", models.CharField(max_length=255)),
                ("validation_status", models.CharField(max_length=255)),
                (
                    "related_products",
                    models.ManyToManyField(blank=True, to="base.product"),
                ),
            ],
            options={
                "unique_together": {("code", "type")},
            },
        ),
    ]
