from rest_framework import serializers

from base.models.product import Product


class ProductListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"


class ProductCreateSerializer(serializers.ModelSerializer):
    trade_item_unit_descriptor = serializers.CharField(required=False)
    trade_item_descriptor = serializers.CharField(required=False)

    class Meta:
        model = Product
        fields = "__all__"


class AmountSerializer(serializers.Serializer):
    amount = serializers.IntegerField()
    bbd = serializers.DateTimeField()
    comment = serializers.CharField(allow_blank=True)
    country_of_disassembly = serializers.CharField(allow_null=True)
    country_of_rearing = serializers.CharField(allow_null=True)
    country_of_slaughter = serializers.CharField(allow_null=True)
    cutting_plant_registration = serializers.CharField(allow_null=True)
    item = ProductCreateSerializer()
    lot_number = serializers.CharField(allow_null=True)
    slaughterhouse_registration = serializers.CharField(allow_null=True)


class AmountsSerializer(serializers.Serializer):
    amounts = AmountSerializer(many=True)
    session_end_time = serializers.DateTimeField()
    session_id = serializers.IntegerField()
    session_start_time = serializers.DateTimeField()
    supplier_id = serializers.CharField()
    user_id = serializers.EmailField()
