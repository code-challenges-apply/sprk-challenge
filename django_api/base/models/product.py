import uuid
import unicodedata

from django.db import models


class Product(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    amount_multiplier = models.IntegerField()
    brand = models.CharField(max_length=255)
    categ_id = models.IntegerField()
    category_id = models.CharField(max_length=255)
    code = models.CharField(max_length=14)
    description = models.TextField()
    edeka_article_number = models.CharField(max_length=255)
    gross_weight = models.FloatField()
    net_weight = models.FloatField()
    notes = models.BooleanField()
    packaging = models.CharField(max_length=255)
    related_products = models.ManyToManyField("self", blank=True)
    requires_best_before_date = models.BooleanField()
    requires_meat_info = models.BooleanField()
    trade_item_unit_descriptor = models.CharField(max_length=255)
    trade_item_unit_descriptor_name = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    unit_name = models.CharField(max_length=255)
    validation_status = models.CharField(max_length=255)

    class Meta:
        unique_together = (("code", "type"),)

    def save(self, *args, **kwargs):
        if self.code.startswith("0"):
            self.code = str(int(self.code))

        # Normalize all string fields
        for field in self._meta.fields:
            if isinstance(field, models.CharField) or isinstance(
                field, models.TextField
            ):
                value = getattr(self, field.name)
                if value is not None:
                    value = unicodedata.normalize("NFC", value)
                    setattr(self, field.name, value)

        super(Product, self).save(*args, **kwargs)

    def __str__(self):
        return f"{self.code} ({self.type})"
