from rest_framework.routers import DefaultRouter

from base.views import ProductViewSet


urlpatterns = []

router = DefaultRouter()
router.register("product", ProductViewSet, basename="product")

urlpatterns += router.urls
