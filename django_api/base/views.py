from rest_framework.viewsets import ModelViewSet

from base.models.product import Product
from base.serializers import (
    ProductListSerializer,
    ProductCreateSerializer,
    AmountsSerializer,
)


class ProductViewSet(ModelViewSet):
    serializer_class = ProductListSerializer
    # permission_classes = (IsAuthenticated,)
    http_method_names = ["get", "post"]

    def get_queryset(self):
        queryset = Product.objects.all()
        code = self.request.query_params.get("code")
        if code is not None:
            queryset = queryset.filter(code=code)
        return queryset

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return self.serializer_class
        elif self.action == "create":
            return AmountsSerializer
        else:
            return self.serializer_class

    def perform_create(self, serializer):
        for amount in serializer.validated_data["amounts"]:
            item = amount["item"]
            if "trade_item_descriptor" in item and item["trade_item_descriptor"]:
                item["trade_item_unit_descriptor"] = item["trade_item_descriptor"]
                del item["trade_item_descriptor"]

            product_serializer = ProductCreateSerializer(data=item)
            product_serializer.is_valid(raise_exception=True)
            product_serializer.save()
