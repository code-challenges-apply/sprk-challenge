from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIRequestFactory
from rest_framework import status
from .models.product import Product
from .views import ProductViewSet


class ProductViewSetTestCase(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()

    def test_perform_create(self):
        self.assertEqual(Product.objects.count(), 0)
        view = ProductViewSet.as_view({"post": "create"})
        data = {
            "amounts": [
                {
                    "amount": 3,
                    "bbd": "2021-09-30T00:00:00Z",
                    "comment": "",
                    "country_of_disassembly": None,
                    "country_of_rearing": None,
                    "country_of_slaughter": None,
                    "cutting_plant_registration": None,
                    "item": {
                        "amount_multiplier": 24,
                        "brand": "Unknown",
                        "categ_id": 750,
                        "category_id": "10000236",
                        "code": "53115271277711",
                        "description": "Waln\u00fcsse idS 30mm+ FR I 500g BT",
                        "edeka_article_number": "80062875",
                        "gross_weight": 0,
                        "id": "2434",
                        "net_weight": 12000,
                        "notes": False,
                        "packaging": "PUG",
                        "related_products": [],
                        "requires_best_before_date": True,
                        "requires_meat_info": False,
                        "trade_item_descriptor": "CASE",
                        "trade_item_unit_descriptor_name": "Karton",
                        "type": "gtin",
                        "unit_name": "g",
                        "validation_status": "validated",
                    },
                    "lot_number": None,
                    "slaughterhouse_registration": None,
                }
            ],
            "session_end_time": "2022-04-29T11:56:37.132Z",
            "session_id": 1651233397132,
            "session_start_time": "2022-04-29T11:40:14.860Z",
            "supplier_id": "1050",
            "user_id": "adrian@sprk.global",
        }
        request = self.factory.post(reverse("product-list"), data=data)
        response = view(request)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)
